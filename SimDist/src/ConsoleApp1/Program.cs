﻿using System;
using SimDist;

namespace CalcSemDist
{
    public class CalcSemDist
    {
        public static void Main(string[] args)
        {
            var syns = new SynonymList();
            string word;

            do
            {
                Console.WriteLine("Введите слово:");
                word = Console.ReadLine();
                var res = syns.SinonymFind(word);
                foreach (var item in res)
                    Console.WriteLine(item.Key + "=" + item.Value);
                Console.WriteLine("\n");
            } while (word != null);
        }
    }
}