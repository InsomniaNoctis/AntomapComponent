﻿using System.Collections.Generic;

namespace SimDist
{
    public class SynonymList
    {
        public Dictionary<string, double> SinonymFind(string word)
        {
            var th = new SynonymSearchEngine(word);
            var res = th.SynonymFind(word);
            return th.SortedListSyn(res);
        }
    }
}