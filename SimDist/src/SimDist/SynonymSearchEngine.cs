﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Office.Interop.Word;
using SimDist.Adapters;
using SimDist.Interfaces;

namespace SimDist
{
    internal class SynonymSearchEngine : ISynonymSearchEngine
    {
        private const double Diametr = 1.0;
        private readonly Dictionary<string, double> dict = new Dictionary<string, double>();
        private readonly Application wordApp = new Application {Visible = false};

        internal SynonymSearchEngine(string word)
        {
            Word = word;
            Adapter = new ThesAdapter("dic.xlsx");
            List = Adapter.Domains;
            Calculate = new SemanticAffinityCalculator(Adapter);
        }

        internal string Word { get; private set; }

        internal Dictionary<string, double> Dic { get; private set; }

        internal List<string> List { get; }

        internal SemanticAffinityCalculator Calculate { get; }

        internal ThesAdapter Adapter { get; }

        public Dictionary<string, double> SynonymFind(string word)
        {
            var infosyns = wordApp.SynonymInfo[word, WdLanguageID.wdRussian];
            var enumerable = infosyns.MeaningList as Array;

            if (enumerable != null)
                foreach (var iitem in enumerable)
                foreach (var item in infosyns.SynonymList[iitem] as Array)
                    if (List.Contains(item.ToString()) &&
                        !dict.ContainsKey(item.ToString())) // есть ли слово в ядре
                    {
                        var simdist = Calculate.SumSim(word, item.ToString());

                        if (simdist >= 0.7 && simdist < 1.0)

                            dict.Add(item.ToString(), Math.Round(simdist,3));
                        SynonymOfSynonym(word, item.ToString());
                    }
            return dict;
        }

        public void SynonymOfSynonym(string word, string syns)
        {
            var simdistsyns = 0.0000;
            var infosynsofsyns = wordApp.SynonymInfo[syns, WdLanguageID.wdRussian];
            var enumerablesyns = infosynsofsyns.MeaningList as Array;
            if (enumerablesyns != null)
                foreach (var synsiitem in enumerablesyns)
                foreach (var synsitem in infosynsofsyns.SynonymList[synsiitem] as Array)
                    if (List.Contains(synsitem.ToString()) &&
                        !dict.ContainsKey(synsitem.ToString())) // есть ли слово в ядре
                    {
                        var firstsimdist = Calculate.SumSim(syns, synsitem.ToString());
                        if (firstsimdist >= 0.7 && firstsimdist < 1.0)
                        {
                            simdistsyns = Calculate.SumSim(word, synsitem.ToString());
                            if (simdistsyns >= 0.7 && simdistsyns < 1.0)
                            {
                                
                                dict.Add(synsitem.ToString(), Math.Round(simdistsyns,3));
                            }
                        }
                    }
        }


        public Dictionary<string, double> SortedListSyn(Dictionary<string, double> dict)
        {
            // сортировка ассоциативного массива
            return dict.OrderByDescending(pair => pair.Value)
                .ToDictionary(pair => pair.Key, pair => pair.Value);
        }
    }
}