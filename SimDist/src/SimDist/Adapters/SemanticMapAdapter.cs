﻿using MathNet.Numerics.Data.Matlab;
using MathNet.Numerics.LinearAlgebra;

namespace SimDist.Adapters
{
    internal class SemanticMapAdapter
    {
        internal SemanticMapAdapter(string file, string variable)
        {
            Coords = MatlabReader.Read<double>(file, variable);
        }

        internal Matrix<double> Coords { get; private set; }
    }
}