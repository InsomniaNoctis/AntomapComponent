﻿using System.Collections.Generic;
using System.IO;
using OfficeOpenXml;

namespace SimDist.Adapters
{
    internal class ThesAdapter
    {
        internal ThesAdapter(string file)
        {
            CreateDictionary(file);
        }

        internal List<string> Domains { get; } = new List<string>();

        private void CreateDictionary(string file)

        {
            var newFile = new FileInfo(file);
            // вытаскиваем слова из "ядра" в лист
            var ws = new ExcelPackage(newFile).Workbook.Worksheets["dic"];
            for (var rw = 1; rw <= ws.Dimension.End.Row; rw++)
                if (ws.Cells[rw, 1].Value != null)
                    Domains.Add(ws.Cells[rw, 1].Value.ToString());
        }
    }
}