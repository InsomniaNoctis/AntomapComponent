﻿using System.Collections.Generic;

namespace SimDist.Interfaces
{
    internal interface ISynonymSearchEngine
    {
        Dictionary<string, double> SynonymFind(string word);
        void SynonymOfSynonym(string word, string syns);
        Dictionary<string, double> SortedListSyn(Dictionary<string, double> dict);
    }
}