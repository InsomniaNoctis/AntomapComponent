﻿namespace SimDist.Interfaces
{
    internal interface ISemanticAffinityCalculator
    {
        double CalculateSimWeight(double[] wordc, double[] syns);
        double SumSim(string word, string syns);
        double[] CoordsFind(string word, int index);
    }
}