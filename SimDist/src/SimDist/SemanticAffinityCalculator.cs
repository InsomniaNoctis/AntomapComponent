﻿using System;
using System.Collections.Generic;
using System.Linq;
using MathNet.Numerics;
using MathNet.Numerics.LinearAlgebra;
using SimDist.Adapters;
using SimDist.Interfaces;

namespace SimDist
{
    internal class SemanticAffinityCalculator : ISemanticAffinityCalculator
    {
        private static readonly double Diametr = 1.0;
        private readonly Matrix<double> _coords;
        private readonly List<string> _domains;
        private readonly double[] pc_sd = {0.81, 0.45, 0.3, 0.22, 0.05, 0.02};

        internal SemanticAffinityCalculator(ThesAdapter adapter)
        {
            CoordAdapter = new SemanticMapAdapter("rus4a.mat", "x");
            _coords = CoordAdapter.Coords;
            _domains = adapter.Domains;
        }

        internal SemanticMapAdapter CoordAdapter { get; set; }


        public double SumSim(string word, string syns)
        {
            var sim1 = CalculateSimWeight(CoordsFind(word, 0), CoordsFind(syns, 0));
            return 1 - sim1;
        }

        public double CalculateSimWeight(double[] wordc, double[] syns)
        {
            var result = 0.000;
            double coef = 1;
            for (var i = 0; i < 6; i++)
                result = result + 1 / Math.Pow(pc_sd[i], 2) * Math.Pow(wordc[i] - syns[i], 2);

            return Math.Pow(result, 0.5);
        }

        public double[] CoordsFind(string word, int index)
        {
            return _coords.Row(_domains.IndexOf(word) + 1).Take(6).ToArray();
        }


    }
}
